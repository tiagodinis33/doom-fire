#include <cstdlib>
#include <iostream>
#include <memory.h>
#include <random>
#include <raylib.h>
#include <string>
#define WIDTH 50
#define HEIGHT 50
#define WINDOW_SCALAR 10

typedef int intensity_t;
typedef unsigned char color_t;
intensity_t fireMaxIntensity = 36;
intensity_t fireSourceIntensity = fireMaxIntensity;
intensity_t fire[WIDTH * (HEIGHT+1)];// Its HEIGHT+1 because it needs to store the light source
Color colors[37] = {
    {7,   7,   7, 255},   {31,  7,   7, 255},
    {47,  15,  7, 255},   {71,  15,  7, 255},
    {87,  23,  7, 255},   {103, 31,  7, 255},
    {119, 31,  7, 255},   {143, 39,  7, 255},
    {159, 47,  7, 255},   {175, 63,  7, 255},
    {191, 71,  7, 255},   {199, 71,  7, 255},
    {223, 79,  7, 255},   {223, 87,  7, 255},
    {223, 87,  7, 255},   {215, 95,  7, 255},
    {215, 95,  7, 255},   {215, 103, 15, 255},
    {207, 111, 15, 255},  {207, 119, 15, 255},
    {207, 127, 15, 255},  {207, 135, 23, 255},
    {199, 135, 23, 255},  {199, 143, 23, 255},
    {199, 151, 31, 255},  {191, 159, 31, 255},
    {191, 159, 31, 255},  {191, 167, 39, 255},
    {191, 167, 39, 255},  {191, 175, 47, 255},
    {183, 175, 47, 255},  {183, 183, 47, 255},
    {183, 183, 55, 255},  {207, 207, 111, 255},
    {223, 223, 159, 255}, {239, 239, 199, 255},
    {255, 255, 255, 255}
};
bool wind = true;

Color getColorByIntensity(intensity_t i) { return colors[i]; }
void updateFireIntensityPerPixel(int currentPixelIndex) {
  auto belowPixelIndex = currentPixelIndex + WIDTH;
  if (belowPixelIndex >= WIDTH * HEIGHT) {
    return;
  }
  auto decay = rand() % 3;
  auto belowPixelFireIntensity = fire[belowPixelIndex];
  auto newFireIntensity = belowPixelFireIntensity - decay >= 0
                              ? belowPixelFireIntensity - decay
                              : 0;
  fire[currentPixelIndex - (wind ? decay : 0)] = newFireIntensity;
}
void calculateFirePropagation() {
  for (int column = 0; column < WIDTH; column++) {
    for (int row = 0; row < HEIGHT; row++) {
      auto pixelIndex = column + (WIDTH * row);
      updateFireIntensityPerPixel(pixelIndex);
    }
  }
}
void renderFire() {
  for (int column = 0; column < WIDTH; column++) {
    for (int row = 0; row < HEIGHT; row++) {
      auto pixelIndex = column + (WIDTH * row);
      DrawRectangle(column * WINDOW_SCALAR, row * WINDOW_SCALAR, WINDOW_SCALAR,
                    WINDOW_SCALAR, getColorByIntensity(fire[pixelIndex]));
    }
  }
}
void createFireSource() {
  for (int i = 0; i <= WIDTH; i++) {
    auto overflowPixelIndex = WIDTH * HEIGHT;
    auto pixelIndex = (overflowPixelIndex - WIDTH) + i;
    fire[pixelIndex] = fireSourceIntensity;
  }
  std::cout << "Creating fire source with intensity of " << fireSourceIntensity
            << std::endl;
}
void resetAlgorithm() {
  for (int i = 0; i < WIDTH * HEIGHT; i++) {
    fire[i] = 0;
  }
  createFireSource();
}
int main() {
  memset(fire, 0, WIDTH * HEIGHT * sizeof(intensity_t));
  createFireSource();
  srand(GetTime());
  InitWindow(WIDTH * WINDOW_SCALAR, HEIGHT * WINDOW_SCALAR, "Doom fire");
  SetTargetFPS(30);
  while (!WindowShouldClose()) {
    // Update
    calculateFirePropagation();
    int key;
    while ((key = GetKeyPressed())) {
      switch (key) {
      case KEY_W:
        wind = !wind;
        break;
      }
    }
    if (IsKeyDown(KEY_UP)) {
      fireSourceIntensity = std::min(fireSourceIntensity + 1, fireMaxIntensity);
      resetAlgorithm();
    }

    if (IsKeyDown(KEY_DOWN)) {
      fireSourceIntensity = std::max(fireSourceIntensity - 1, 0);
      resetAlgorithm();
    }
    // Draw
    BeginDrawing();
    renderFire();
    EndDrawing();
  }
  CloseWindow();
}
